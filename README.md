# README #

Author: Jacob Brown, jbrown14@uoregon.edu
Project 3, 10/29/18, for CIS 322, Fall 2018

Description: An anagram game that utilizes Flask, JSON, AJAX, Docker to create an anagram game. Search through the jumble to find any words listed at the top. Find the number of words specified and win!

Instructions: Make sure a "credentials.ini" file exists in the vocab directory first. Next, in the vocab directory containing the Dockerfile, use "run.sh" to build and start this program. The game will appear at localhost:5000 in a web browser.
