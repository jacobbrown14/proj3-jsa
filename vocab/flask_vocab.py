"""
Flask web site with vocabulary matching game
(identify vocabulary words that can be made 
from a scrambled string)

Edited by Jacob Brown, 10/29/2018
for Project 3 (CIS 322, Fall 2018)
"""

import flask
import logging

# Our own modules
from letterbag import LetterBag
from vocab import Vocab
from jumble import jumbled
import config

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY  # Should allow using session variables

# One shared 'Vocab' object, read-only after initialization,
# shared by all threads and instances.  Otherwise we would have to
# store it in the browser and transmit it on each request/response cycle,
# or else read it from the file on each request/responce cycle,
# neither of which would be suitable for responding keystroke by keystroke.

WORDS = Vocab(CONFIG.VOCAB)

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    """The main page of the application"""
    flask.g.vocab = WORDS.as_list()
    flask.session["target_count"] = min(
        len(flask.g.vocab), CONFIG.SUCCESS_AT_COUNT)
    flask.session["jumble"] = jumbled(
        flask.g.vocab, flask.session["target_count"])
    flask.session["matches"] = []
    app.logger.debug("Session variables have been set")
    assert flask.session["matches"] == []
    assert flask.session["target_count"] > 0
    app.logger.debug("At least one seems to be set correctly")
    return flask.render_template('vocab.html')


###############
# AJAX request handlers
#   These return JSON, rather than rendering pages.
###############


# check() - this function grabs text from vocab.html
# and checks whether it counts as a valid word in the
# current anagram/jumble and word list.
@app.route("/_check")
def check():
    
    # Grabs text in form "attempt" from vocab.html
    text = flask.request.args.get("text", type=str)

    # Grabs jumble, matches, and target count
    jumble = flask.session["jumble"]
    matches = flask.session.get("matches", []) # default to empty list
    target_count = flask.session["target_count"]

    # Used to verify if letters exist in current jumble
    # and constitute a valid word from the list
    in_jumble = LetterBag(jumble).contains(text)
    matched = WORDS.has(text)

    # Boolean for readability
    all_good = matched and in_jumble and not (text in matches)

    # Blank response for each call to this function
    resp = " "

    # Responds accordingly
    if all_good:
        # Adds new word to matches
        matches.append(text)
        flask.session["matches"] = matches
        resp = "{} has been found!".format(text)
    elif text in matches:
        resp = "You've already found {}.".format(text)
    elif not matched:
        resp = "{} isn't in the list of words.".format(text)
    elif not in_jumble:
        resp = "{} can't be made from these letters.".format(text)
    else:
        assert False # Raises AssertionError

    # If we hit the target count of words, we change the response
    if len(matches) >= target_count:
        resp = "You've found {} words! Congrats!".format(target_count)

    # This gathers everything for the AJAX stuff in vocab.html
    rslt = {"valid_entry" : all_good, "response" : resp, "matches" : matches}
    return flask.jsonify(result = rslt)

###################
#   Error handlers
###################

@app.errorhandler(404)
def error_404(e):
    app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404

@app.errorhandler(500)
def error_500(e):
    app.logger.warning("++ 500 error: {}".format(e))
    assert not True  # I want to invoke the debugger
    return flask.render_template('500.html'), 500

@app.errorhandler(403)
def error_403(e):
    app.logger.warning("++ 403 error: {}".format(e))
    return flask.render_template('403.html'), 403

####

if __name__ == "__main__":
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0")
